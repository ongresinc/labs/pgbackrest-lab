# pgbackrest

## setup

Install vagrant and virtualbox:

```bash
brew cask install virtualbox vagrant
```

## how it works

This lab contains details about how to setup pgbackrest on your local machine.

the `Makefile` contains all steps necessary. It basically does:

* `up` to create the machines
* `down` to shutdown the machines
* `clean` to remove the machines and clean up the `~/.know_hosts` file
* `deploy` to execute the `postgres` and `pgbackrest` playbooks that will install:
    * 2 postgres servers (`10.0.0.11` and `10.0.0.20`) with `PostgreSQL 12.2`
    * 1 backup server `10.0.0.20` with Pgbackrest `2.24`
> Check the `inventory.yml` file for details.

## fast setup

To create the servers and setup them, just type:

```bash
make up deploy
```
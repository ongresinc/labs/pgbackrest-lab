deploy:
	@cd ansible ; ansible-playbook -i ../inventory.yml postgres.yml
	@cd ansible ; ansible-playbook -i ../inventory.yml pgbackrest.yml

up:
	@export VAGRANT_NO_PLUGINS=1 && vagrant up

down:
	@vagrant down

clean:
	@vagrant destroy -f
	@sed -i .backup '/10.0.0./d' ~/.ssh/known_hosts
